import React, { Component } from 'react';
// import FacebookLogin from 'react-facebook-login';
import './App.css';

class App extends Component {
  state = {
    isConnected: null,
    pages: null
  };

  componentDidMount() {
    window.FB.getLoginStatus(response => {
      console.log(response);

      if (response.status === 'connected') {
        this.setState({ isConnected: true });
      } else {
        this.setState({ isConnected: false });
      }
    });
  }

  updatePagesData = response => {
    console.log(response);
    this.setState({
      pages: response.data
    });
  };

  getListPages = () => {
    window.FB.api('/me/accounts', 'GET', {}, this.updatePagesData);
  };

  loginFB() {
    window.FB.login(
      response => {
        console.log(response);
      },
      {
        scope:
          'email,manage_pages,pages_messaging,pages_messaging_subscriptions,pages_show_list'
      }
    );
  }

  logoutFB() {
    window.FB.logout(response => {
      console.log('Logout :', response);
    });
  }

  pageSubscribe(pageId, accessToken) {
    // window.FB.api(`/${pageId}/subscribed_apps`, function(response) {
    //   if (response && !response.error) {
    //     console.log(response);
    //   }
    // });
    window.FB.api(
      `/${pageId}/subscribed_apps?access_token=${accessToken}`,
      'POST',
      {},
      function(response) {
        console.log(response);
      }
    );
  }

  render() {
    const { isConnected, pages } = this.state;

    return (
      <div className="container">
        <div className="card my-5">
          <div className="card-body">
            {isConnected ? (
              <div>
                <h1>Hello</h1>
                <button
                  className="btn btn-secondary mb-4"
                  onClick={this.logoutFB}
                >
                  Logout
                </button>
              </div>
            ) : (
              <button className="btn btn-primary" onClick={this.loginFB}>
                Login with Facebook
              </button>
            )}
            {isConnected && (
              <button className="btn btn-primary" onClick={this.getListPages}>
                Get List Page
              </button>
            )}
            <ul className="list-group mt-4">
              {pages &&
                pages.map(item => (
                  <li className="list-group-item d-flex justify-content-between align-items-center" key={item.id}>
                    <span>{item.name}</span>
                    <button
                      className="btn btn-primary btn-small"
                      onClick={() => {
                        this.pageSubscribe(item.id, item.access_token);
                      }}
                    >
                      Connect
                    </button>
                  </li>
                ))}
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
